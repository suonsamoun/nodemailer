var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

var transporter = nodemailer.createTransport(({
    service: 'gmail',
    port: 25,
    auth: {
        user: 'sender email address',
        pass: 'sender email password'
    }
}));

// send mail
transporter.sendMail({
    from: 'sender',
    to: 'receiver email',
    subject: 'This email sent by using Node.js',
    text: 'Hello this is a sample text message.',
    html: '<b>Hello world 🐴</b>'
}, function(error, response) {
   if (error) {
        throw error;
   } else {
        console.log('Message has been sent successfully!');
   }
});
